**Life Cycle**:

<img src="https://gitlab.com/alen_guler/alenhb/uploads/de9409e30d990454155abcbb011b64da/Untitled_Diagram.jpg" alt="Build Status">

**Used Techs.**:
    
    * Nodejs for application
    * Docker for containerization
    * Gitlab for Ci/Cd and version controlling
    * GKE for Kubernetes
    * DockerHub for container registry


**Files**:
    
    1. index.js: port and output message are set.
    2. Dockerfile: node application was containerized with this Dockerfile.
    3. gitlab-service-account.yaml: gitlab service account that is used to deploy to Kubernetes from GitLab was created with this yaml.
    4. deployment.yaml: deployment, service and 
    5. .gitlab-ci.yaml: ci/cd is made with this file. 
        * Creating image and pushing step:
            - Docker login with variables that are stored in gitlav variables.
            - Docker build with versioning.
            - Docker push to private dockerhub container registry.
        * Kubernetes deploy:
            - Service account created with applying gitlab-service-account.yaml.
            - Cluster properties were set with kubectl config set-* commands.
            - Deployment, service and autoscaling settings deployed with deployment.yaml.


**Notes**:

    1. Nodejs is used because of making small as much as possible. Port is directed to 11130 and output message is set as wanted.
    2. When dockerizing nodejs app with base node image, image was approximately 1GB. To reduce image size;
        * "multi stage docker builds" method was used.
        * node12-alpine image was used. 
        After these steps, image size reduces 900MB and became approximately 100MB.
    3. In deployment.yaml replicas are set as 3. Autoscaling option is set with "targetAverageUtilization" option. When there will be less then 5       targetAverageUtilization pods will be auto scaled 3 to 9 one by one.
    4. Gitlab is used as SAAS not self managed because of making challenge faster. Project variables (username, password, etc. variables) are stored    here for make project more secure. 
    5. GKE is used because of 300$ free usage of Google Cloud and configured via console not with any infra. code like Terraform.
    6. Private dockerhub is used for container registry.
    7. Builds are versioned with system variable of gitlab (CI_PIPELINE_ID).
    8. System files that should not stored in git repo are set with .dockerignore and .gitignore file.

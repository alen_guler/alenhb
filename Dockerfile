FROM node:12 AS BUILD_IMAGE

WORKDIR /app

COPY . .

EXPOSE 11130

RUN npm install 

FROM node:12-alpine

WORKDIR /app

# copy from build image
COPY --from=BUILD_IMAGE /app/index.js ./


CMD node ./index.js
const http = require("http");
const port = 11130;

const server = http.createServer((req, res) => {

  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello Hepsiburada from Alen Guler\n');
});

server.listen(port, () => {
  console.log(`Server running at ${port}/`);
});